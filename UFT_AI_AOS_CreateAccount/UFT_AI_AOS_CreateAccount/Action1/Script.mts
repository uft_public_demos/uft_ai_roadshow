﻿' This is a simple example test using UFT AI to show how to create an account on Advantage Online Shopping

'  These variables are set from the global data sheet and are used for readability
Dim userName, email, password, firstName, lastName, phoneNumber, country, city, address, state, postalCode

' Launch the browser (this can be parameterized to run on different browsers)
webutil.LaunchBrowser "CHROME"
Set myBrowser = Browser("creationtime:=0")
myBrowser.Maximize

' Set the focus of UFT AI to the browser we've just opened
AIUtil.SetContext myBrowser

' Navigate to the AOS website
myBrowser.Navigate "https://www.advantageonlineshopping.com/#/"

' Click on the my profile icon
AIUtil("profile").Click

' Click on the create new account link
AIUtil.FindTextBlock("CREATE NEW ACCOUNT").Click

' Validate we are on the CREATE ACCOUNT page
AIUtil.FindText("CREATE ACCOUNT").CheckExists True
' AIUtil.FindTextBlock("CREATE ACCOUNT", micFromBottom, 1).CheckExists True

' Enter user creation  Account Details
AIUtil("input", "Username").Type "AI_User_001"
AIUtil("input", "Email").Type "AI_User_001@foulkconsulting.com"
AIUtil("input", "Password").Type "Password123"
AIUtil("input", "Confirm password").Type "Password123"

' Enter user creation Personal Details
AIUtil("input", "First Name").Type "Robot"
AIUtil("input", "Last Name").Type "Man"
AIUtil("input", "Phone Number").Type "212 555-1234"

