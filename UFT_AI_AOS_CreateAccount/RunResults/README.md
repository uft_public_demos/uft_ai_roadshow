# UFT Run Results will be stored here

The run results will be generated during the build pipeline and saved to this directory and then saved as an artifact. If integrating with a test management solution we can use APIs or OOB configs to log the tests to the testing tool of our choice.