import subprocess

test_path = ".\\UFT_AI_AOS_CreateAccount\\UFT_AI_AOS_CreateAccount"
results_path = "C:\\Temp\\UFT_Parallel_Results\\UFT_AI_AOS_CreateAccount"

run_command = f'ParallelRunner -t "{test_path}" -b "Chrome" -r "{results_path}" -rn "ValidateCreateAccount"'

print(run_command)

subprocess.run(run_command, shell=True)